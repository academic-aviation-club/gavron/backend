import datetime
import json

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Point(models.Model):
    coords_x = models.FloatField()
    coords_y = models.FloatField()


class Drone(models.Model):
    name = models.TextField()


class FlightRoute(models.Model):
    drone_route = models.TextField(blank=False, default='')

    def __str__(self):
        return json.loads(self.drone_route)["name"]


class FlightSchedule(models.Model):
    schedule_start_date = models.DateTimeField('first flight')
    schedule_minutes_interval = models.IntegerField()
    route = models.ForeignKey(FlightRoute, on_delete=models.CASCADE)

    def __str__(self):
        # It's dirty AF ikr. But I REALLY don't want to deal with the fucking timezones
        time_to_flight_seconds = (datetime.datetime.today() - self.schedule_start_date.replace(tzinfo=None)).total_seconds()
        when_info = f'{(-time_to_flight_seconds):.3} seconds from now' if time_to_flight_seconds <= 0 else f'{time_to_flight_seconds:.3} seconds ago'
        return f'Flight scheduled at {self.schedule_start_date.time()} UTC ({when_info}). Route name: {self.route}'

    # scheduleId = models.IntegerField()
    # scheduleStartDate = models.DateTimeField('flight time')
    # scheduleMinutesInterval = models.IntegerField()
    # drone_route = models.ForeignKey(FlightRoute, on_delete=models.CASCADE)


class Flight(models.Model):
    drone = models.ForeignKey(Drone, on_delete=models.CASCADE)
    schedule = models.ForeignKey(FlightSchedule, on_delete=models.CASCADE)
    date = models.DateTimeField('flight date')


class Telemetry(models.Model):
    '''
    Temporarily disabling those 2 fields, because I wan't to 
    test backing up the telemetry to the server
    '''
    # drone = models.ForeignKey(Drone, on_delete=models.CASCADE)
    # schedule = models.ForeignKey(FlightSchedule, on_delete=models.CASCADE)
    telemetry = models.TextField('telemetry data')


# class Drone(models.Model):
#     drone_number = models.IntegerField(blank=False)
#     curr_pos_lat = models.FloatField(default=0)
#     curr_pos_lon = models.FloatField(default=0)
#     curr_pos_alt = models.FloatField(default=0)
#     battery = models.IntegerField(default=0)
#     last_signal = models.DateTimeField('last_signal')
