#websockets/consumers.py
import json
from channels.generic.websocket import WebsocketConsumer
from websockets.authorization import handle_auth_event
from websockets.active_consumers import ActiveConsumer
import os

#every connection
class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
        handle_auth_event(self) #testing auth, to delete

    def disconnect(self, close_code):
        ActiveConsumer.remove(self)

    def receive(self, json_data):
        self.send(json_data)