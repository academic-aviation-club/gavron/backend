#websockets.active_consumers
import json

#authorized consumer
class ActiveConsumer:
    active_connections = []

    @staticmethod
    def clear():
        for connection in ActiveConsumer.active_connections:
            connection.close()

    @staticmethod
    def add(connection):
        if connection not in ActiveConsumer.active_connections:
            ActiveConsumer.active_connections.append(connection)
            print(ActiveConsumer.active_connections)

    @staticmethod
    def remove(connection):
        if connection in ActiveConsumer.active_connections:
            ActiveConsumer.active_connections.remove(connection)
            print(ActiveConsumer.active_connections)

    @staticmethod
    def send_to_all(json_data):
        for connection in ActiveConsumer.active_connections:
            connection.receive(json_data)