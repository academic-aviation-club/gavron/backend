from django.contrib import admin
from .models import FlightRoute, FlightSchedule

admin.site.register(FlightRoute)
admin.site.register(FlightSchedule)

