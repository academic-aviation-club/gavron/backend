"""backendWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.conf.urls import include, url
from rest_framework.authtoken import views as vs
from django.urls import path
from rest_framework import routers, serializers, viewsets
from django.contrib.auth.models import User
from schedules import views


router = routers.DefaultRouter()
router.register(r'route', views.FlightRouteViewSet)
router.register(r'flight-schedule', views.FlightScheduleViewSet)
router.register(r'drone', views.DroneViewSet)
router.register(r'point', views.PointViewSet)
router.register(r'flight', views.FlightViewSet)
router.register(r'telemetry', views.TelemetryViewSet)

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^api-token-auth', vs.obtain_auth_token),
    url(r'^chat/', include('websockets.urls')),
]

urlpatterns += router.urls
