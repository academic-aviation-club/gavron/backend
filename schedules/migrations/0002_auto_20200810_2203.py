# Generated by Django 3.0.8 on 2020-08-10 22:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0001_initial'),
    ]

    operations = [
        # migrations.CreateModel(
        #     name='FlightRoute',
        #     fields=[
        #         ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
        #         ('drone_route', models.TextField(default='')),
        #     ],
        # ),
        # migrations.DeleteModel(
        #     name='Routes',
        # ),

        migrations.RenameModel(
            old_name='Schedule',
            new_name='FlightSchedule',
        ),migrations.RenameModel(
            old_name='Routes',
            new_name='FlightRoute',
        ),
        migrations.AlterField(
            model_name='flightschedule',
            name='drone_route',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='schedules.FlightRoute'),
        ),
    ]
