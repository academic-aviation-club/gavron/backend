import logging
from rest_framework.decorators import action
from schedules.models import FlightRoute, FlightSchedule, Drone, Point, Flight, Telemetry
from schedules.serializers import RoutesSerializer, SchedulesSerializer, \
    DroneSerializer, PointSerializer, FlightSerializer, TelemetrySerializer
from rest_framework import viewsets, status
from rest_framework.mixins import CreateModelMixin, ListModelMixin, \
    RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from dronestemp.drone_handler import DroneHandler

logger = logging.getLogger()


class PointViewSet(CreateModelMixin, ListModelMixin,
                   RetrieveModelMixin, UpdateModelMixin,
                   DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = PointSerializer
    queryset = Point.objects.all()


class FlightRouteViewSet(CreateModelMixin, ListModelMixin,
                         RetrieveModelMixin, UpdateModelMixin,
                         DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = RoutesSerializer
    queryset = FlightRoute.objects.all()


class FlightScheduleViewSet(CreateModelMixin, ListModelMixin,
                            RetrieveModelMixin, UpdateModelMixin,
                            DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = SchedulesSerializer
    queryset = FlightSchedule.objects.all()


class FlightViewSet(CreateModelMixin, ListModelMixin,
                    RetrieveModelMixin, UpdateModelMixin,
                    DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = FlightSerializer
    queryset = Flight.objects.all()


class TelemetryViewSet(CreateModelMixin, ListModelMixin,
                       RetrieveModelMixin, UpdateModelMixin,
                       DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = TelemetrySerializer
    queryset = Telemetry.objects.all()

    # drone_handler = DroneHandler()
    # examplary request to test with dronekit sitl
    # r = requests.post('http://127.0.0.1:8000/drone/1/drone_telemetry/',
    #                   json={"drone_number": 2, "curr_pos_lat": lat,
    #                         "curr_pos_lon": lon, "curr_pos_alt": alt,
    #                         "battery": battery, "last_signal": str(last_signal)})


class DroneViewSet(CreateModelMixin, ListModelMixin,
                   RetrieveModelMixin, UpdateModelMixin,
                   DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = DroneSerializer
    queryset = Drone.objects.all()
    # accept post request from drone with telemetry

    # @action(detail=True, methods=['post'])
    # def drone_telemetry(self, request, pk=None):
    #     # drone = self.get_object()
    #     serializer = DroneSerializer(data=request.data)

    #     if serializer.is_valid():
    #         drone_number = serializer.data['drone_number']
    #         curr_pos_lat = serializer.data['curr_pos_lat']
    #         curr_pos_lon = serializer.data['curr_pos_lon']
    #         curr_pos_alt = serializer.data['curr_pos_alt']
    #         battery = serializer.data['battery']
    #         last_signal = serializer.data['last_signal']

    #         drone_handler.receive_data(
    #             drone_number, curr_pos_lat, curr_pos_lon, curr_pos_alt, battery)
    #         drone_handler.send_data()
    #         # uncomment to show working
    #         # print('Curr lat: ', curr_pos_lat, ' curr lon: ', curr_pos_lon, ' curr alt: ', curr_pos_alt,
    #         #       ' battery: ', battery, ' last signal: ', last_signal)
    #         #
    #         # print(drone_handler.get_drone(2).get_telem())
    #         return Response(serializer.data)
    #     else:
    #         return Response(serializer.errors,
    #                         status=status.HTTP_400_BAD_REQUEST)
