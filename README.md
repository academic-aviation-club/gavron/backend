# Drone monitoring backend web

## Manually:

**Create virtual environment:**
  
```
virtualenv venv
```

**Install requirements:**

```
pip install -r requirements.txt
```

**Make migrations**

```
python manage.py makemigrations
python manage.py migrate
```

**To run server:**

```
python manage.py runserver
```

**With docker**

```
docker-compose build
docker-compose up
```


## Connect to websocket:
`
WebSocket("ws://" + url)  # default url: 127.0.0.1:8000
`

## JSON data format:
```
JSON_data = {
            "id": self.id,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "altitude": self.altitude,
            "battery": self.batt,
   }
```
