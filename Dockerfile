FROM python:3

ENV PYTHONUNBUFFERED 1
  
# create root directory for our project in the container
RUN mkdir /gavron_backend

# Set the working directory to /backend_service
WORKDIR /gavron_backend

# Copy the current directory contents into the container at /backend_service
ADD . /gavron_backend

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt 
