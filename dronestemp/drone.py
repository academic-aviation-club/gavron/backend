#websockets/drone.py
import datetime
import json

# TODO: timezone and seconds_to_offline in config file
SECONDS_TO_OFFLINE = 3


class Drone:
    def __init__(self, id: int):
        self.id = id
        self.latitude = None
        self.longitude = None
        self.altitude = None
        self.last_signal = None
        self.batt = None

    def update_telem(self, latitude: float, longitude: float, altitude: float, batt: int):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.batt = batt
        self.last_signal = datetime.datetime.now()

    def is_online(self) -> bool:
        current = datetime.datetime.now()
        timedelta = current - self.last_signal
        return timedelta.total_seconds() <= SECONDS_TO_OFFLINE
    
    def get_telem(self) -> json:
        #no offline/online in json, server dont send telem when drone is offline
        telem = {
            "id": self.id,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "altitude": self.altitude,
            "battery": self.batt,

        }
        telem_to_send = {"message": json.dumps(telem)}
        return json.dumps(telem_to_send)