from schedules.models import FlightRoute, FlightSchedule, Drone, Point, Flight, Telemetry
from rest_framework import serializers


class PointSerializer(serializers.ModelSerializer):

    class Meta:
        model = Point
        fields = (
            'id',
            'coords_x',
            'coords_y',
        )


class RoutesSerializer(serializers.ModelSerializer):

    class Meta:
        model = FlightRoute
        fields = (
            'id',
            'drone_route'
        )


class SchedulesSerializer(serializers.ModelSerializer):

    class Meta:
        model = FlightSchedule
        fields = (
            'id',
            'schedule_start_date',
            'schedule_minutes_interval',
            'route',
        )


class TelemetrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Telemetry
        fields = (
            # 'drone',
            # 'schedule',
            'telemetry',
        )

# class SchedulesDroneSerializer(serializers.ModelSerializer):
#     drone_route = serializers.SlugRelatedField(
#         read_only=True,
#         slug_field='drone_route'
#     )

#     class Meta:
#         model = FlightSchedule
#         fields = (
#             'id',
#             'drone_number',
#             'scheduled_time',
#             'drone_route'
#         )


class DroneSerializer(serializers.ModelSerializer):

    class Meta:
        model = Drone
        fields = (
            'id',
            'name',
        )


class FlightSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flight
        fields = (
            'id',
            'drone',
            'schedule',
            'date',
        )
