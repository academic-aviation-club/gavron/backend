from dronestemp.drone import Drone
from websockets import active_consumers

# 👌😂


class DroneHandler:
    drones_set = {}

    @staticmethod
    def clear():
        DroneHandler.drones_set.clear()

    @staticmethod
    def add_drone(drone_id: int):
        if drone_id not in DroneHandler.drones_set:
            DroneHandler.drones_set[drone_id] = Drone(drone_id)
    
    @staticmethod
    def remove_drone(drone_id: int):
        if drone_id in DroneHandler.drones_set:
            del DroneHandler.drones_set[drone_id]

    @staticmethod
    def get_drone(drone_id: int) -> Drone:
        if drone_id in DroneHandler.drones_set:
            return DroneHandler.drones_set[drone_id]
    
    #update data from udp getter function
    @staticmethod
    def receive_data(drone_id: int, latitude: float, longitude: float, altitude: float, batt: int):
        if drone_id not in DroneHandler.drones_set:
            DroneHandler.add_drone(drone_id)
        DroneHandler.get_drone(drone_id).update_telem(latitude, longitude, altitude, batt)

    #send current data to clients from online drones
    @staticmethod
    def send_data():
        for drone_id in DroneHandler.drones_set:
            drone = DroneHandler.get_drone(drone_id)
            if drone.is_online():
                active_consumers.ActiveConsumer.send_to_all(drone.get_telem())